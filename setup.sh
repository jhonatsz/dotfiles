#!/bin/bash

HOMEDIR="$(pwd)"


### VIM SETUP

# Download plug.vim if 
if [ ! -f "$HOMEDIR/.vim/autoload/plug.vim" ]; then
  echo "Downloading vim-plug"
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs \
      https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
else
  echo "Plugin already exist"
fi

# Create/Update Sylmink of .vimrc
if [ -L "$HOMEDIR/.vimrc" ]; then
    echo "Updating Symlink .vimrc"
    rm "$HOMEDIR/.vimrc"
    ln -s $HOMEDIR/.dotfiles/vimrc $HOMEDIR/.vimrc
else
  echo "Symlinking .vimrc"
  ln -s $HOMEDIR/.dotfiles/vimrc $HOMEDIR/.vimrc
fi

# Running PlugInstall for vimrc plugins
echo "PlugInstall Executed"
vim -c "PlugInstall" +qa
### END

### TMUX SETUP
# Create/Update Sylmink of .tmux.conf
if [ -L "$HOMEDIR/.tmux.conf" ]; then
    echo "Updating Symlink .tmux.conf"
    rm "$HOMEDIR/.tmux.conf"
    ln -s $HOMEDIR/.dotfiles/tmux.conf $HOMEDIR/.tmux.conf
else
  echo "Symlinking .tmux.conf"
  ln -s $HOMEDIR/.dotfiles/tmux.conf $HOMEDIR/.tmux.conf
fi

# Create/Update Sylmink of .tmux.conf.local
if [ -L "$HOMEDIR/.tmux.conf.local" ]; then
    echo "Updating Symlink .tmux.conf.local"
    rm "$HOMEDIR/.tmux.conf.local"
    ln -s $HOMEDIR/.dotfiles/tmux.conf.local $HOMEDIR/.tmux.conf.local
else
  echo "Symlinking .tmux.conf.local"
  ln -s $HOMEDIR/.dotfiles/tmux.conf.local $HOMEDIR/.tmux.conf.local
fi
### END



# Create/Update Sylmink of .bash_profile
if [ -L "$HOMEDIR/.bash_profile" ]; then
    echo "Updating Symlink .bash_profile"
    rm "$HOMEDIR/.bash_profile"
    ln -s $HOMEDIR/.dotfiles/bash_profile $HOMEDIR/.bash_profile
else
  echo "$HOMEDIR/.bash_profile"
  echo "Symlinking .bash_profile"
  ln -s $HOMEDIR/.dotfiles/bash_profile $HOMEDIR/.bash_profile
fi


# Create/Update Sylmink of .bash_aliases
if [ -L "$HOMEDIR/.bash_aliases" ]; then
    echo "Updating Symlink .bash_aliases"
    rm "$HOMEDIR/.bash_aliases"
    ln -s $HOMEDIR/.dotfiles/bash_aliases $HOMEDIR/.bash_aliases
else
  echo "Symlinking .bash_aliases"
  ln -s $HOMEDIR/.dotfiles/bash_aliases $HOMEDIR/.bash_aliases
fi

# Create/Update .bashrc
if [ -L "$HOMEDIR/.bashrc" ]; then
    echo "Updating Symlink .bashrc"
    rm "$HOMEDIR/.bashrc"
    ln -s $HOMEDIR/.dotfiles/bashrc $HOMEDIR/.bashrc
  else
  echo "Symlinking .bashrc"
  rm "$HOMEDIR/.bashrc"
  ln -s $HOMEDIR/.dotfiles/bashrc $HOMEDIR/.bashrc
fi

# Create/Update .gitconfig
if [ -L "$HOMEDIR/.gitconfig" ]; then
  echo "Updating Symlink .gitconfig"
  rm "$HOMEDIR/.gitconfig"
  ln -s $HOMEDIR/.dotfiles/gitconfig $HOMEDIR/.gitconfig
else
  echo "Symlinking .gitconfig"
  rm "$HOMEDIR/.gitconfig"
  ln -s $HOMEDIR/.dotfiles/gitconfig $HOMEDIR/.gitconfig
fi


