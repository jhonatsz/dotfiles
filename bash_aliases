# List of Personal Aliases

# Homebrew Aliases
alias brewup='brew update; brew upgrade; brew cleanup; brew doctor'

# Git Aliases
alias gs='git status'
alias gf='git diff'
alias gl='git log --pretty=oneline'


# Terraform Aliases
#alias terraform='docker run --rm -i -t -v $(pwd):/app/ -w /app -v $HOME/.aws:/root/.aws hashicorp/terraform'


# AWSCLI Aliases
#alias aws='docker run --rm -tiv $HOME/.aws:/root/.aws anigeo/awscli'


# PostgreSQL Aliases
alias pg_start="launchctl load ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"
alias pg_stop="launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.postgresql.plist"

