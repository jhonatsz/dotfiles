# My Personal .dotfiles

## Major Requirements

- Homebrew: download here: [Homebrew Website](https://brew.sh/).

## Setting up and Symlinking your .dotfiles environment
- `bash setup.sh`

## Brew Commands

### Updating your Brewfile
- `brew bundle dump --force --describe`

## List of Applications that doesnt saved on Brewfile

### DevOps Tooling
- Docker
- Virtualbox
- Vagrant
- Ansible   #Installed via pyenv/pip
- Terraform #Downloaded from browser and installed

### Version Management Tooling
- Pyenv
- Nodenv
- Rbenv

# Productivity Tools
- Macdown
